docker-acars

Docker image to get ACARS (Aircraft Communications Addressing and Reporting System) messages for Raspberry Pi with RTL-SDR USB stick.


Build it.
`docker build -t docker-acars .`

Run/Watch it.
`docker run -it --device=/dev/bus/usb/001/005 acarsdec`