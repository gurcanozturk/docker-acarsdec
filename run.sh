#!/bin/sh

# Parameters for acarsdec
# -v: verbose
# -g: tuner gain (450 = 45db)
# -r: tuner number
#  131.725 ACARS freuency in Europe
#  131.550 ACARS generic frequency
#
/usr/bin/acarsdec -v -g 450 -r 0 131.725 131.550