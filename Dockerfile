FROM alpine:latest

RUN apk update && apk add --no-cache --repository \
     'http://dl-cdn.alpinelinux.org/alpine/edge/testing' librtlsdr-dev && \
    apk add --no-cache \
     make \
     cmake \
     gcc \
     musl-dev \
     ncurses-dev \
     ncurses-libs \
     binutils \
     bash

RUN  apk add --no-cache git && cd /tmp && git clone https://github.com/TLeconte/acarsdec.git

RUN  cd /tmp/acarsdec && mkdir build && cd build && \
     cmake .. -Drtl=ON && make && cp acarsdec /usr/bin/ && \
     cd /tmp && rm -rf acarsdec* && rm -rf /var/cache/apk/*


COPY run.sh /
RUN chmod +x /run.sh
CMD ["/run.sh"]